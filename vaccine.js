const got = require('got');

const districtCode = {
    571: `Chennai`,
    539: `Coimbatore`
}
const getVaccineData = async (age, district) => {
    const data  = await got.get(`https://cdn-api.co-vin.in/api/v2/appointment/sessions/calendarByDistrict?district_id=${district}&date=08-05-2021`);
    const { centers } = JSON.parse(data.body);
    const available_centers = [];
    if(centers) {
        centers.map(center=> {
            const { sessions } = center;
            if(sessions) {
                sessions.map((session)=> {
                    const { min_age_limit, available_capacity } = session;
                    if(min_age_limit === age && available_capacity >0 ){ 
                        available_centers.push(available_capacity);
                    }
                })
            }
        })
    }
    console.log(available_centers.length);
    if(available_centers.length > 0) {
        const count = available_centers.reduce(function (a, b) {
            return a + b; 
        })
        const message = `THERE ARE ${count} available slots in district: ${districtCode[district]}. Book a slot ASAP`;
        await got.post('https://discord.com/api/webhooks/840599578323583036/NO9AQq786GzuQvQeMSMN8UNskUODnA8wqU5FjyYkmiw1EdyQjG1ESxKXt5Igo0aXnEz_', {
            json: {
                content: message,
            },
            responseType: 'json'
        });
    }
}

const scheduledRun = async () => {
    await getVaccineData(18, 571); //chennai
    await getVaccineData(18, 539); //coimbatore
}

//Test Run
getVaccineData(45, 539)

setInterval(async ()=> {await scheduledRun()}, 600000); // 10 mins once.